<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once "partials/_head.php";?>
    <link href="<?php echo $this->_helpers->linkTo("css/swiper.css", "Assets")?>" type="text/css" rel="stylesheet"/>
    <title>Kitec - Crea, comparte y cobra</title>
    <style>
        .StripeElement {
            background-color: white;
            height: 40px;
            padding: 10px 12px;
            border-radius: 4px;
            border: 1px solid transparent;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }
    </style>
</head>
<body style="overflow-x:hidden;">
    <?php require_once "partials/_wel_menu.php";?>
    <header class="valign-wrapper" style="position: relative;height:40em;background:url(<?php echo $this->_helpers->linkTo("img/home-wall.png", "Assets")?>);background-size:cover;margin-bottom:2.5em;">
        <div style="background: url(<?php echo $this->_helpers->linkTo("img/login-stuff-0.png", "Assets")?>) 0% 0% rgba(37, 23, 132,0.55);background-size: cover;background-repeat: no-repeat;position: absolute;top: 0;left: 0;width: 100%;height: 100%;"></div>
        <div class="container" style="z-index:1;">
            <div class="row">
                <div class="col s12 m7">
                    <h3 style="font-size:2rem;font-family: Multicolore;font-weight: 600;color:#FFF;line-height: 0.9;">Nuevas formas de <strong class="kstrong">vender</strong></h3>
                    <p style="font-size: 1.3rem;color:#FFF;">
                        Con solo "Copiar y Pegar" cobras donde quieras.<br>
                        <span style="font-weight:300;">Sea que vendas en línea, en las redes sociales, en tu tienda o desde el maletero de tu auto.</span>
                    </p>
                    <a href="<?php echo $this->_helpers->linkToPlatform("accounts/reg")?>" class="btn k-btn waves-effect waves-dark red white-text">crea tu cuenta</a>
                    <a class="btn k-btn waves-effect waves-dark white purple-text">Contacta al equipo de ventas</a>
                </div>
            </div>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="row">
                <div class="col s12 m12 center-align">
                    <h3 style="font-size:2rem;color:#5f537d;font-family:multicolore;">La mejor manera de tener tu negocio en internet</strong></h3>
                    <p style="font-weight: 400;font-size: 17px;line-height: 26px;color: #6b7c93;margin:0 auto;width:90%;text-align:justify;">
                        Sabemos lo complicado que es cobrar por internet en nuestros pa&iacute;ses, y lo dif&iacute;cil que puede ser convencer a nuestros clientes de ir hasta nuestro sitio web para obtener uno de nuestros productos.<br><br>
                        Por eso creemos que cobrar en Internet debe ser simple; para nosotros tener pagos en l&iacute;nea es cosa de tan solo tres pasos: <strong>Crea, comparte y vende.</strong>
                        Y nos encanta que nuestros clientes vendan donde quieran y cuando quieran.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="swiper-cursos swiper-container" style="padding:50px 10px 20px;width:100%;">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/checkout-ico.png", "Assets")?>) -28% 42% #ff1d6e;background-size: 50%;background-repeat: no-repeat;">
                            <div class="row" style="padding:20px 15px;">
                                <div class="col s12 m9 offset-m3 white-text right-align">
                                    <h5 style="font-size:1.3rem;margin-top:3px;line-height:1.3;font-family: multicolore;">Pagos sin enredos</h5>
                                    <div style="display:inline-block;margin-top:5px;font-size:0.9rem;">
                                        <ul>
                                            <li>Acepta pagos en l&iacute;nea ahora.</li>
                                            <li>Olv&iacute;date de los programadores y los sitios web.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/payment-ico.png", "Assets")?>) -28% 42% #9d85f7;background-size: 50%;background-repeat: no-repeat;">
                            <div class="row" style="padding:20px 15px;">
                                <div class="col s12 m8 offset-m4 white-text right-align">
                                    <h5 style="font-size:1.3rem;margin-top:3px;line-height:1.3;font-family: multicolore;">Diversidad de Tarjetas</h5>
                                    <div style="display:inline-block;margin-top:5px;font-size:0.9rem;">
                                        <ul>
                                            <li>Tus clientes pueden pagar con cualquier tarjeta de cr&eacute;dito o d&eacute;bito.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/safe-ico.png", "Assets")?>) -28% 42% #ffdb01;background-size: 50%;background-repeat: no-repeat;">
                            <div class="row" style="padding:20px 15px;">
                                <div class="col s12 m8 offset-m4 white-text right-align">
                                    <h5 style="font-size:1.3rem;margin-top:3px;line-height:1.3;font-family: multicolore;">Est&aacute;s protegido</h5>
                                    <div style="display:inline-block;margin-top:5px;font-size:0.9rem;">
                                        <ul>
                                            <li>Realiza cobros con confianza, tenemos a los mejores cuidando de ti.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Pagination -->
                <div class="swiper-pagination" style="margin-top:50px;"></div>
            </div>
        </div>
        <div class="row white-text" style="background: url(<?php echo $this->_helpers->linkTo("img/lines-circles.png", "Assets")?>) 50% 50% #2a004b;background-size: contain;padding: 60px 10px;">
            <div class="container">
                <div class="col s12 m12 center-align">
                    <h3 style="font-size:2rem;font-family:multicolore;">Tus clientes est&aacute;n en <strong class="kstrong">redes sociales</strong></strong></h3>
                    <p style="font-weight: 400;font-size: 17px;line-height: 26px;margin:0 auto;width:90%;">
                        Dales el placer de pagarte sin salir de sus redes.
                    </p>
                </div>
                <div style="display: inline-block;width: 100%;margin: 4em 0 0;">
                    <div class="demo-chat col s12 m4 black-text" style="margin-bottom:20px;">
                        <div style="background:#eaeaea;border-radius:7px;min-height:19.5em;">
                            <div class="bubble b-right">
                                <span>¡Me encanta ese sueter 😍!<br>¿C&oacute;mo te lo compro?</span>
                            </div>
                            <div class="bubble b-left animated bounceIn">
                                <span>Pagame aqu&iacute; 😉</span><br>
                                <a href="<?php echo $this->_helpers->linkToPlatform("ticket/pay/rkatB-hBG")?>" target="_blank"><span>kitecweb.com/rkatB-hBG</span></a>
                                <a href="<?php echo $this->_helpers->linkToPlatform("ticket/pay/rkatB-hBG")?>" target="_blank">
                                    <div class="bubble-link">
                                        <div class="thumb-logo">
                                            <img src="<?php echo $this->_helpers->linkTo("img/icons/thumb-logo.png", "Assets")?>" class="responsive-img" style="object-fit:cover;border-radius:50%;">
                                        </div>
                                        <span style="position:relative;top:1px;">$50.35 • Sueter - Pagame con kitec</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="demo-pay col s12 m4 offset-m4 black-text">
                        <div style="background:#eaeaea;border-radius:7px;min-height:19.5em;">
                            <div style="float: right;display: inline-block;position: relative;right: -40px;top: 20px;opacity: 0.88;">
                                <div class="col s12 m12 center-align" style="padding:0;margin-bottom:10px;">
                                    <h2 class="flow-text purple-text" style="margin:0;font-weight:600;font-size:3rem;font-family:rubik;">$50.35</h2>
                                </div>
                                <div class="col s12 m12" style="padding:0;margin-bottom: 10px;min-height:44px;">
                                    <div class="valign-wrapper hovertext" style="background:#ececec;border-radius:25px;width:100%;float:left;padding:10px;cursor:pointer;box-shadow: -10px 10px 0px rgba(0, 0, 0, 0.16);">
                                        <div style="width:89%;float:left;padding:0 0.75rem;line-height:1.2;">
                                            <div class="title-bill first" style="font-weight:500;">Sueter pro</div>
                                            <span class="second" style="font-size:0.8rem;color:#6f6f6f;">Click para m&aacute;s info.</span>
                                        </div>
                                        <div class="right" style="width:11%;">
                                            <select>
                                                <option value="1" selected>x1</option>
                                                <option value="2">x2</option>
                                                <option value="3">x3</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col s12 m12" style="margin: 0 0 20px 0;padding: 0;">
                                    <label for="card_user">N&uacute;mero de Tarjeta</label>
                                    <div id="card-element" class="email-singup ticket-card-input" style="cursor:text;box-shadow: -10px 10px 0px rgba(0, 0, 0, 0.16);">
                                        <!-- a Stripe Element will be inserted here. -->
                                    </div>
                                    <div id="card-errors" role="alert"></div>
                                </div>
                                <div class="col s12 m12" style="padding:0;">
                                    <a id="send" class="send btn btn-flat big-purple-button waves-effect waves-light white-text" data-view="mobile-login" style="box-shadow: -10px 10px 0px rgba(0, 0, 0, 0.16);">Pagar ahora</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin:50px 0;">
                <div class="col s12 m7 left-align">
                    <h3 style="font-size:2rem;color:#5f537d;font-family:multicolore;margin-top:0;">Productos</h3>
                    <p style="font-weight: 400;font-size: 17px;line-height: 26px;color: #6b7c93;text-align:left;">
                        Hemos puesto todo nuestro esfuerzo para brindarte las mejores herramientas, nuestros productos te permiten manejar todo respecto a los pagos de tu negocio; no hace falta que desarrolles nada por tu cuenta, nosotros nos encargamos de todo.
                    </p>
                </div>
                <a class="btn k-btn waves-effect waves-dark purple white-text right" style="margin:50px 0 0;">Conoce nuestra oferta</a>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin:50px 0;">
                <div class="col s12 m4" style="padding-left:0;">
                    <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/home/history-ico.png", "Assets")?>) 3% 5% #FFF;background-size:25%;background-repeat: no-repeat;width:100%;">
                        <div class="row black-text" style="padding:10px 15px;">
                            <div class="col s8 m8 valign-wrapper offset-m4 offset-s4" style="height:64px;">
                                <h5 class="red-text" style="font-size:1.6rem;margin:0;line-height:1.3;font-family: multicolore;">Record</h5>
                            </div>
                            <div class="col s12 m12" style="margin-top:5px;">
                                <div style="display:inline-block;font-size:0.9rem;">
                                    <ul>
                                        <li>Almacena todos los tickets de cobro creados, no perder&aacute;s ninguno de vista.</li>
                                        <li>Filtra por productos, modifica y obten la url de cada ticket f&aacute;cilmente.</li>
                                    </ul>
                                </div>
                            </div>
                            <a class="btn btn-flat k-btn waves-effect waves-dark white red-text right" style="margin:10px 0.75rem 0 0;">M&aacute;s informaci&oacute;n</a>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4">
                    <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/home/sales-ico.png", "Assets")?>) 3% 5% #FFF;background-size:25%;background-repeat: no-repeat;width:100%;">
                        <div class="row black-text" style="padding:10px 15px;">
                            <div class="col s8 m8 valign-wrapper offset-m4 offset-s4" style="height:64px;">
                                <h5 class="yellow-text" style="font-size:1.6rem;margin:0;line-height:1.3;font-family: multicolore;">Sales</h5>
                            </div>
                            <div class="col s12 m12" style="margin-top:5px;">
                                <div style="display:inline-block;font-size:0.9rem;">
                                    <ul>
                                        <li>Ent&eacute;rate de los cobros que recibes.</li>
                                        <li>Obt&eacute;n m&eacute;tricas de seguimiento, informaci&oacute;n de tu cliente y detalles sobre el monto cobrado.</li>
                                    </ul>
                                </div>
                            </div>
                            <a class="btn btn-flat k-btn waves-effect waves-dark white yellow-text right" style="margin:10px 0.75rem 0 0;">M&aacute;s informaci&oacute;n</a>
                        </div>
                    </div>
                </div>
                <div class="col s12 m4" style="padding-right:0;">
                    <div class="box-slide" style="background: url(<?php echo $this->_helpers->linkTo("img/icons/home/payment-ico.png", "Assets")?>) 3% 5% #FFF;background-size:25%;background-repeat: no-repeat;width:100%;">
                        <div class="row black-text" style="padding:10px 15px;">
                            <div class="col s8 m8 valign-wrapper offset-m4 offset-s4" style="height:64px;">
                                <h5 class="light-blue-text" style="font-size:1.6rem;margin:0;line-height:1.3;font-family: multicolore;">Payedme</h5>
                            </div>
                            <div class="col s12 m12" style="margin-top:5px;">
                                <div style="display:inline-block;font-size:0.9rem;">
                                    <ul>
                                        <li>Registra tus pagos cobrados en el tiempo.</li>
                                        <li>Administra las fechas para tus pr&oacute;ximos pagos y brinda detalles de estos.</li>
                                        <li>Mantiene tu balance al d&iacute;a.</li>
                                    </ul>
                                </div>
                            </div>
                            <a class="btn btn-flat k-btn waves-effect waves-dark white light-blue-text right" style="margin:10px 0.75rem 0 0;">M&aacute;s informaci&oacute;n</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row" style="margin:50px 0;padding:10px 20px;border-radius:50px;background:#e8e8e8;">
                <div class="col s12 m6 valign-wrapper left-align" style="height:36px;">
                    <h3 style="font-size:1.3rem;color:#7b7b7b;font-family:multicolore;margin:0;">Sin tarifas escondidas</h3>
                </div>
                <a href="<?php echo $this->_helpers->linkTo("pricing")?>" class="btn btn-flat k-btn-simple waves-effect waves-dark right" style="border-radius:50px;">Consulta nuestros precios</a>
            </div>
        </div>
        <div class="row purple" style="background: url(<?php echo $this->_helpers->linkTo("img/lines-circles.png", "Assets")?>) 50% 50%;background-size: contain;margin:0;padding:50px 0;">
            <div class="container">
                <div class="col s12 m7 white-text left-align" style="padding-left:0;">
                    <h3 style="font-size:2rem;font-family:multicolore;margin-top:0;">¿Empezamos?</h3>
                    <p style="font-weight: 400;font-size: 17px;line-height: 26px;text-align:left;">
                        Crea una cuenta sin enredos o contacta con nuestro equipo de ventas.
                    </p>
                </div>
                <a class="btn k-btn waves-effect waves-dark red white-text right" style="margin:0;">crea tu cuenta</a>
                <a class="btn k-btn waves-effect waves-dark white purple-text right" style="margin:20px 0 0;">Contacta al equipo de ventas</a>
            </div>
            <?php require_once "partials/_footer.php";?>
        </div>
    </main>
    <div id="get-started" class="modal" style="width:30%;">
        <div class="modal-content">
            <div class="row s12 m6 center-align" style="padding:0;">
                <img src="<?php echo $this->_helpers->linkTo("img/icons/hello-ico.png", "Assets")?>" style="width:13%;margin-right:5px;">
                <span style="position:relative;top:-17px;font-size:1.5rem;font-weight:600;color:#5a3fda;">Hola, desde kitec</span>
                <a class="modal-action modal-close btn-floating right white" style="width:25px;height:25px;line-height:27px;">
                    <i class="material-icons" style="font-size:0.8rem;line-height:27px;color:gray;">close</i>
                </a>
            </div>
            <div class="row s12 m6" style="margin-bottom:0px;padding-left: 0;">
                <label for="ussr_fld">Correo Electr&oacute;nico</label>
                <input type="text" id="ussr_fld" name="ussrld" class="ussr_fld email-singup" placeholder="@tucorreo.com" style="color: #7a69ec;width:96.5%;"/>
            </div>
            <div class="row s12 m6" style="margin-bottom: 18px;padding-left: 0;">
                <label for="pss_fld">Contrase&ntilde;a</label>
                <input type="password" id="pss_fld" name="pssld" class="pss_fld email-singup" placeholder="Ingresa tu contrase&ntilde;a" style="color: #7a69ec;width:96.5%;"/>
            </div>
            <div class="row s12 m6" style="padding:0;">
                <a class="send btn btn-flat big-purple-button waves-effect waves-light white-text" onclick="login();" style="background-color:#5441d5;">Entrar</a>
            </div>
            <div class="row s12 m6" style="padding:0;margin:10px 0;">
                <hr/>
            </div>
            <div class="row s12 m6" style="padding:0;">
                <a class="btn btn-flat waves-effect waves-light" data-view="mobile-login" style="width:100%;padding: 0px 25px;border-radius: 7px;text-transform: none;">Registrarme</a>
            </div>
            <div class="row s12 m6 center-align" style="padding:0;margin-top:10px;">
                <span>&iquest;Qu&eacute; es kitec? </span><a class="text-lighten-3" style="font-weight:500;color:#000 !important;text-decoration:underline;">Conoce m&aacute;s</a>
            </div>
        </div>
    </div>

    <!--  Scripts-->
    <?php require_once "partials/_outputJs.php";?>
    <script src="<?php echo $this->_helpers->linkTo("js/swiper.min.js", "Assets")?>"></script>
    <script src="https://js.stripe.com/v3/"></script>
    <?php require_once "partials/DOM_funcs/_stripe_ele_init.php";?>
    <script>
        $(document).ready(function () {
            var swiper = new Swiper('.swiper-cursos', {
                slidesPerView: 1,
                spaceBetween: 30,
                loop: true,
                centeredSlides: true,
                pagination: {
                  el: '.swiper-pagination',
                  clickable: true,
                },
            });
        });
    </script>
</body>

</html>