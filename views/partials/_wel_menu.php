<nav class="navbar-fix hide-on-med-and-down" role="navigation" style="position:absolute;background-color:transparent;border-bottom:none;box-shadow:none !important;">
    <div class="nav-wrapper container" style="width:75%;">
        <div class="row right" style="width:100%;">
            <a href="<?php echo $this->_helpers->linkTo("")?>" class="brand-logo">
                <img src="<?php echo $this->_helpers->linkTo("img/logo.png", "Assets")?>" style="position:relative;top:15px;width:12%;">
            </a>
            <ul class="hide-on-med-and-down right" style="padding:10px 0;">
                <li style="padding:0 0.75rem;">
                    <a href="<?php echo $this->_helpers->linkTo("pricing")?>" style="padding: 14px 20px;display: initial;border-radius: 50px;">
                        Tarifas
                    </a>
                </li>
                <li style="padding:0 0.75rem;">
                    <a href="<?php echo $this->_helpers->linkTo("pricing")?>" style="padding: 14px 20px;display: initial;border-radius: 50px;">
                        Empresa
                    </a>
                </li>
                <li style="padding:0 0.75rem;">
                    <a href="<?php echo $this->_helpers->linkToPlatform("")?>" style="padding:10px 20px;border:2px solid #ffbc0f;border-radius: 89px;display: initial;">
                        <img src="<?php echo $this->_helpers->linkTo("img/icons/logout-ico.png", "Assets")?>" style="position: relative;top: 9px;margin-right: 5px;max-width: 26px;width: auto;-moz-transform: scaleX(-1);-o-transform: scaleX(-1);-webkit-transform: scaleX(-1);transform: scaleX(-1);filter: FlipH;-ms-filter: ;">
                            Iniciar sesi&oacute;n
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>