<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>

<!-- CSS  -->
<link rel="icon" type="image/png" href="<?php echo $this->_helpers->linkTo("img/favicon_.png", "Assets")?>"/>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link href="<?php echo $this->_helpers->linkTo("css/materialize.css", "Assets")?>" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="<?php echo $this->_helpers->linkTo("css/style.css", "Assets")?>" type="text/css" rel="stylesheet" media="screen,projection"/>
<link href="<?php echo $this->_helpers->linkTo("css/animate.css", "Assets")?>" type="text/css" rel="stylesheet" media="screen,projection"/>
<style>
    @font-face { font-family: GeosansLight; src: url("<?php echo $this->_helpers->linkTo("fonts/GeosansLight.ttf", "Assets")?>"); }
    @font-face { font-family: Multicolore; src: url("<?php echo $this->_helpers->linkTo("fonts/Multicolore.otf", "Assets")?>"); }
    .kstrong{
        background: url(<?php echo $this->_helpers->linkTo("img/under-line.png", "Assets")?>) 53%;
        background-size: 98% 80%;
        background-repeat: no-repeat;
        color: #fff;
    }
</style>