<footer class="page-footer" style="z-index:1000;background-color:rgba(42, 0, 75, 0.39);">
    <div class="footer-copyright">
        <div class="container">
            <div class="row" style="margin:20px 0;">
                <div class="col s12 m6 hide-on-small-only" style="padding:0;">
                    <a class="white-text" style="margin:0 0.55rem 0 0;">Productos</a>
                    <a class="white-text" style="margin:0 0.55rem;">Precios</a>
                    <a class="white-text" style="margin:0 0.55rem;">Empresa</a>
                    <a class="white-text" style="margin:0 0.55rem;">Inversores</a>
                    <a class="white-text" style="margin:0 0.55rem;">Anuncios</a>
                </div>
                <div class="col s12 m6 right-align" style="padding:0;">
                    Hola desde <strong class="kstrong">El Salvador</strong>&nbsp;&nbsp;|&nbsp;&nbsp;Braincore &copy; 2017 - <?php echo date("Y");?>
                </div>
            </div>
        </div>
    </div>
 </footer>