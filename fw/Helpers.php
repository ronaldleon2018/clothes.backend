<?php
class Helpers extends App{
    
    /**
    * @param String $notify msg
    */
    public function throwErrorPage($notify){
        $notify['class'] = 'NOTIFY::Error[0]';
        $notify['notify_title'] = 'Oh snap!';
        $notify['icon'] = 'NOTIFY::Error[1]';
        require_once APPDIR::Views . 'error.php'; exit;
    }
    
    /**
    * Construye el link complementando con el dominio y protocolo
    */
    public function linkTo($route, $dir = '', $type = ''){
        $d = (empty($dir)) ? '' : APPDIR::getDir($dir);
        $link = (empty($type)) ? $this->BASEDIR : '';
        $link .= $d . $route;
        return $link;
    }

    public function linkToPlatform($route, $dir = '', $type = ''){
        $d = (empty($dir)) ? '' : APPDIR::getDir($dir);
        $link = (empty($type)) ? $this->BASEDIR_PLATFORM : '';
        $link .= $d . $route;
        return $link;
    }

    public function makeId($q=10,$t=''){
        $seed = str_split('abcdefghijklmnopqrstuvwxyz'
                          .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                          .'0123456789'); // and any other characters
        shuffle($seed); // probably optional since array_is randomized; this may be redundant
        $id = '';
        foreach (array_rand($seed, $q) as $k) $id .= $seed[$k];
        return $id.$t;
    }
    
     public function sendMail($subject, $body, $address,$addressName){
        try {
            require_once APPDIR::Libs . 'phpmailer/PHPMailerAutoload.php';
            
            $mail = new PHPMailer;
            
            $mail->isSMTP();                                       // Set mailer to use SMTP
            $mail->Host = EMAIL::Host;                             // Specify main and backup SMTP servers
            $mail->SMTPAuth = EMAIL::SMTPAuth;                     // Enable SMTP authentication
            $mail->Username = EMAIL::Username;                     // SMTP username
            $mail->Password = EMAIL::Password;                     // SMTP password
            $mail->SMTPSecure = EMAIL::SMTPSecure;                 // Enable TLS encryption, `ssl` also accepted
            $mail->Port = EMAIL::Port;                             // TCP port to connect to

            $mail->From = EMAIL::From;
            $mail->FromName = EMAIL::FromName;
            $mail->addAddress(EMAIL::AddAdress, EMAIL::FromName);
            $mail->addReplyTo($address, $addressName);
            $mail->isHTML(true);                                  // Set email format to HTML

            $mail->Subject = $subject;
            $mail->Body = $body;

            $mailResult="";
            if (!$mail->send()) {
                $mailResult .= 'Message could not be sent.';
                $mailResult .= 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                $mailResult = 'Message has been sent to us.';
            }
            $mail = null;

        } catch (PDOException $e) {
            $mailResult .= "DataBase Error.<br>" . $e->getMessage();

        } catch (Exception $e) {
            $mailResult .= "General Error.<br>" . $e->getMessage();
        }
        return $mailResult;
        
    }
}